package pl.kasprowski.controllers;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import pl.kasprowski.services.ImageService;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class ImageController {

    private ImageService imageService;

    @Autowired
    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @GetMapping("/recipe/{recipeId}/image")
    public String showImageForm(@PathVariable Long recipeId, Model model) {
        model.addAttribute("recipeId", recipeId);
        return "recipe/imageuploadform";
    }

    @PostMapping("/recipe/{recipeId}/image")
    public String uploadFile(@PathVariable Long recipeId, @RequestPart("imageFile") MultipartFile imageFile) {
        imageService.saveImage(recipeId, imageFile);
        return "redirect:/recipe/show/" + recipeId;
    }

    @GetMapping("/recipe/{recipeId}/image/show")
    public void getImageByRecipeID(@PathVariable Long recipeId, HttpServletResponse response) throws IOException {
        response.setContentType("image/jpeg");
        InputStream is = new ByteArrayInputStream(imageService.getImage(recipeId));
        IOUtils.copy(is, response.getOutputStream());
    }
}
