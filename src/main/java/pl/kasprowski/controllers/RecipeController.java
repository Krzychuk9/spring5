package pl.kasprowski.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.kasprowski.dto.RecipeDto;
import pl.kasprowski.exeptions.NotFoundException;
import pl.kasprowski.services.RecipeService;

import javax.validation.Valid;

@Controller
public class RecipeController {

    private RecipeService recipeService;

    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @RequestMapping("/recipe/show/{id}")
    public String showById(@PathVariable String id, Model model) {
        model.addAttribute("recipe", recipeService.findById(new Long(id)));
        return "recipe/show";
    }

    @GetMapping("/recipe")
    public String showRecipeForm(Model model) {
        model.addAttribute("recipe", new RecipeDto());
        return "recipe/recipeForm";
    }

    @GetMapping("/recipe/{id}/update")
    public String showRecipeUpdateForm(@PathVariable Long id, Model model) {
        model.addAttribute("recipe", recipeService.findById(id));
        return "recipe/recipeForm";
    }

    @PostMapping("/recipe")
    public String saveRecipeForm(@ModelAttribute("recipe") @Valid RecipeDto recipeDto, BindingResult result) {
        if (result.hasErrors()) {
            return "recipe/recipeForm";
        }
        recipeService.saveRecipe(recipeDto);
        return "redirect:/";
    }

    @GetMapping("/recipe/{id}/delete")
    public String deleteRecipe(@PathVariable Long id) {
        recipeService.delete(id);
        return "redirect:/";
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView handleNotFoundException(Exception ex) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("404error");
        modelAndView.addObject("exception", ex);
        return modelAndView;
    }
}
