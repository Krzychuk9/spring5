package pl.kasprowski.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.kasprowski.domain.*;
import pl.kasprowski.repositories.CategoryRepository;
import pl.kasprowski.repositories.RecipeRepository;
import pl.kasprowski.repositories.UnitOfMeasureRepository;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {

    private RecipeRepository recipeRepository;
    private CategoryRepository categoryRepository;
    private UnitOfMeasureRepository unitOfMeasureRepository;
    private ResourceLoader resourceLoader;

    @Autowired
    public DataInitializer(RecipeRepository recipeRepository, CategoryRepository categoryRepository,
                           UnitOfMeasureRepository unitOfMeasureRepository, ResourceLoader resourceLoader) {
        this.recipeRepository = recipeRepository;
        this.categoryRepository = categoryRepository;
        this.unitOfMeasureRepository = unitOfMeasureRepository;
        this.resourceLoader = resourceLoader;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initRecipe();
        log.debug("Data initialized!");
    }

    private void initRecipe() {
        Recipe recipe = new Recipe();
        recipe.setDescription("Spicy Grilled Chicken Tacos Recipe");
        recipe.setPreTime(15);
        recipe.setCookTime(60);
        recipe.setServings(4);
        recipe.setSource("Chicken source");
        recipe.setUrl("http://www.simplyrecipes.com/recipes/spicy_grilled_chicken_tacos/");
        recipe.setDirections("directions");

        try {
            Resource resource = resourceLoader.getResource(ResourceLoader.CLASSPATH_URL_PREFIX + "static/guacamole400x400.jpg");
            byte[] bytes = Files.readAllBytes(resource.getFile().toPath());
            recipe.setImage(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Notes notes = new Notes();
        notes.setRecipeNotes("recipe chicken notes");
        recipe.setNotes(notes);

        Set<Ingredient> ingredients = new HashSet<>();
        Ingredient ingredient1 = new Ingredient();
        ingredient1.setDescription("small corn tortillas");
        ingredient1.setAmount(new BigDecimal("8"));
        ingredient1.setRecipe(recipe);
        Optional<UnitOfMeasure> apiece = unitOfMeasureRepository.findByUom("apiece");
        apiece.ifPresent(ingredient1::setUnitOfMeasure);
        ingredients.add(ingredient1);

        Ingredient ingredient2 = new Ingredient();
        ingredient2.setDescription("packed baby arugula");
        ingredient2.setAmount(new BigDecimal("3"));
        ingredient2.setRecipe(recipe);
        Optional<UnitOfMeasure> cup = unitOfMeasureRepository.findByUom("Cup");
        cup.ifPresent(ingredient2::setUnitOfMeasure);
        ingredients.add(ingredient2);

        Ingredient ingredient3 = new Ingredient();
        ingredient3.setDescription("cherry tomatoes");
        ingredient3.setAmount(new BigDecimal("0.5"));
        ingredient3.setRecipe(recipe);
        Optional<UnitOfMeasure> pint = unitOfMeasureRepository.findByUom("pint");
        pint.ifPresent(ingredient3::setUnitOfMeasure);
        ingredients.add(ingredient3);

        recipe.setIngredients(ingredients);


        Set<Category> categories = new HashSet<>();
        Optional<Category> category = categoryRepository.findByCategoryName("Mexican");
        category.ifPresent(categories::add);
        recipe.setCategories(categories);

        recipe.setDifficulty(Difficulty.MODERATE);

        recipeRepository.save(recipe);

        recipe = new Recipe();
        recipe.setDescription("How to Make Perfect Guacamole Recipe");
        recipe.setPreTime(25);
        recipe.setCookTime(30);
        recipe.setServings(4);
        recipe.setSource("Guacamole source");
        recipe.setUrl("http://www.simplyrecipes.com/recipes/perfect_guacamole/");
        recipe.setDirections("directions");

        try {
            Resource resource = resourceLoader.getResource(ResourceLoader.CLASSPATH_URL_PREFIX + "static/tacos400x400.jpg");
            byte[] bytes = Files.readAllBytes(resource.getFile().toPath());
            recipe.setImage(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

        notes = new Notes();
        notes.setRecipeNotes("recipe guacamole notes");
        recipe.setNotes(notes);

        ingredients = new HashSet<>();
        ingredient1 = new Ingredient();
        ingredient1.setDescription("avocados");
        ingredient1.setAmount(new BigDecimal("2"));
        ingredient1.setRecipe(recipe);
        apiece = unitOfMeasureRepository.findByUom("apiece");
        apiece.ifPresent(ingredient1::setUnitOfMeasure);
        ingredients.add(ingredient1);

        ingredient2 = new Ingredient();
        ingredient2.setDescription("Kosher salt");
        ingredient2.setAmount(new BigDecimal("0.5"));
        ingredient2.setRecipe(recipe);
        Optional<UnitOfMeasure> teaspoon = unitOfMeasureRepository.findByUom("Teaspoon");
        teaspoon.ifPresent(ingredient2::setUnitOfMeasure);
        ingredients.add(ingredient2);

        ingredient3 = new Ingredient();
        ingredient3.setDescription("cilantro");
        ingredient3.setAmount(new BigDecimal("2"));
        ingredient3.setRecipe(recipe);
        Optional<UnitOfMeasure> tablespoon = unitOfMeasureRepository.findByUom("Tablespoon");
        tablespoon.ifPresent(ingredient3::setUnitOfMeasure);
        ingredients.add(ingredient3);

        recipe.setIngredients(ingredients);


        categories = new HashSet<>();
        Optional<Category> italian = categoryRepository.findByCategoryName("Italian");
        italian.ifPresent(categories::add);
        recipe.setCategories(categories);

        recipe.setDifficulty(Difficulty.MODERATE);

        recipeRepository.save(recipe);
    }
}
