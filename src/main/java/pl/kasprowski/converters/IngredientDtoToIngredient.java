package pl.kasprowski.converters;

import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.kasprowski.domain.Ingredient;
import pl.kasprowski.dto.IngredientDto;

@Component
public class IngredientDtoToIngredient implements Converter<IngredientDto, Ingredient> {

    private UnitOfMeasureDtoToUnitOfMeasure uomConverter;

    @Autowired
    public IngredientDtoToIngredient(UnitOfMeasureDtoToUnitOfMeasure uomConverter) {
        this.uomConverter = uomConverter;
    }

    @Synchronized
    @Nullable
    @Override
    public Ingredient convert(IngredientDto dto) {
        if (dto == null) {
            return null;
        }
        final Ingredient ingredient = new Ingredient();
        ingredient.setId(dto.getId());
        ingredient.setDescription(dto.getDescription());
        ingredient.setAmount(dto.getAmount());
        ingredient.setUnitOfMeasure(uomConverter.convert(dto.getUnitOfMeasure()));
        return ingredient;
    }
}
