package pl.kasprowski.converters;

import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.kasprowski.domain.Recipe;
import pl.kasprowski.dto.RecipeDto;

@Component
public class RecipeToRecipeDto implements Converter<Recipe, RecipeDto> {

    private IngredientToIngredientDto ingredientConverter;
    private NotesToNotesDto notesConverter;
    private CategoryToCategoryDto categoryConverter;

    @Autowired
    public RecipeToRecipeDto(IngredientToIngredientDto ingredientConverter, NotesToNotesDto notesConverter, CategoryToCategoryDto categoryConverter) {
        this.ingredientConverter = ingredientConverter;
        this.notesConverter = notesConverter;
        this.categoryConverter = categoryConverter;
    }

    @Synchronized
    @Nullable
    @Override
    public RecipeDto convert(Recipe recipe) {
        if (recipe == null) {
            return null;
        }
        RecipeDto dto = new RecipeDto();
        dto.setId(recipe.getId());
        dto.setDescription(recipe.getDescription());
        dto.setPreTime(recipe.getPreTime());
        dto.setCookTime(recipe.getCookTime());
        dto.setServings(recipe.getServings());
        dto.setSource(recipe.getSource());
        dto.setUrl(recipe.getUrl());
        dto.setDirections(recipe.getDirections());
        dto.setDifficulty(recipe.getDifficulty());
        if (recipe.getNotes() != null) {
            dto.setNotes(notesConverter.convert(recipe.getNotes()));
        }
        if (recipe.getIngredients() != null) {
            recipe.getIngredients().stream().map(ingredientConverter::convert).forEach(dto.getIngredients()::add);
        }
        if (recipe.getCategories() != null) {
            recipe.getCategories().stream().map(categoryConverter::convert).forEach(dto.getCategories()::add);
        }
        return dto;
    }
}
