package pl.kasprowski.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.kasprowski.domain.Notes;
import pl.kasprowski.dto.NotesDto;

@Component
public class NotesToNotesDto implements Converter<Notes, NotesDto> {

    @Synchronized
    @Nullable
    @Override
    public NotesDto convert(Notes notes) {
        if (notes == null) {
            return null;
        }
        final NotesDto dto = new NotesDto();
        dto.setId(notes.getId());
        dto.setRecipeNotes(notes.getRecipeNotes());
        return dto;
    }
}
