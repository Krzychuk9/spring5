package pl.kasprowski.converters;

import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.kasprowski.domain.Recipe;
import pl.kasprowski.dto.RecipeDto;

import java.util.HashSet;

@Component
public class RecipeDtoToRecipe implements Converter<RecipeDto, Recipe> {

    private IngredientDtoToIngredient ingredientConverter;
    private NotesDtoToNotes notesConverter;
    private CategoryDtoToCategory categoryConverter;

    @Autowired
    public RecipeDtoToRecipe(IngredientDtoToIngredient ingredientConverter, NotesDtoToNotes notesConverter, CategoryDtoToCategory categoryConverter) {
        this.ingredientConverter = ingredientConverter;
        this.notesConverter = notesConverter;
        this.categoryConverter = categoryConverter;
    }

    @Synchronized
    @Nullable
    @Override
    public Recipe convert(RecipeDto dto) {
        if (dto == null) {
            return null;
        }
        Recipe recipe = new Recipe();
        recipe.setId(dto.getId());
        recipe.setDescription(dto.getDescription());
        recipe.setPreTime(dto.getPreTime());
        recipe.setCookTime(dto.getCookTime());
        recipe.setServings(dto.getServings());
        recipe.setSource(dto.getSource());
        recipe.setUrl(dto.getUrl());
        recipe.setDirections(dto.getDirections());
        recipe.setDifficulty(dto.getDifficulty());
        if (dto.getNotes() != null) {
            recipe.setNotes(notesConverter.convert(dto.getNotes()));
        }
        if (dto.getIngredients() != null) {
            recipe.setIngredients(new HashSet<>());
            dto.getIngredients().stream().map(ingredientConverter::convert).forEach(i -> {
                i.setRecipe(recipe);
                recipe.getIngredients().add(i);
            });
        }
        if (dto.getCategories() != null) {
            recipe.setCategories(new HashSet<>());
            dto.getCategories().stream().map(categoryConverter::convert).forEach(recipe.getCategories()::add);
        }
        return recipe;
    }
}
