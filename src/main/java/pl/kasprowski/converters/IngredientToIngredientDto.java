package pl.kasprowski.converters;

import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.kasprowski.domain.Ingredient;
import pl.kasprowski.dto.IngredientDto;

@Component
public class IngredientToIngredientDto implements Converter<Ingredient, IngredientDto> {

    private UnitOfMeasureToUnitOfMeasureDto uomConverter;

    @Autowired
    public IngredientToIngredientDto(UnitOfMeasureToUnitOfMeasureDto uomConverter) {
        this.uomConverter = uomConverter;
    }

    @Synchronized
    @Nullable
    @Override
    public IngredientDto convert(Ingredient ingredient) {
        if (ingredient == null) {
            return null;
        }
        final IngredientDto dto = new IngredientDto();
        dto.setId(ingredient.getId());
        if (ingredient.getRecipe() != null) {
            dto.setRecipeId(ingredient.getRecipe().getId());
        }
        dto.setDescription(ingredient.getDescription());
        dto.setAmount(ingredient.getAmount());
        dto.setUnitOfMeasure(uomConverter.convert(ingredient.getUnitOfMeasure()));
        return dto;
    }
}
