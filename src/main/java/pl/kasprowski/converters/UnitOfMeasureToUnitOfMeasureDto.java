package pl.kasprowski.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.kasprowski.domain.UnitOfMeasure;
import pl.kasprowski.dto.UnitOfMeasureDto;

@Component
public class UnitOfMeasureToUnitOfMeasureDto implements Converter<UnitOfMeasure, UnitOfMeasureDto> {

    @Synchronized
    @Nullable
    @Override
    public UnitOfMeasureDto convert(UnitOfMeasure uom) {
        if (uom == null) {
            return null;
        }
        final UnitOfMeasureDto dto = new UnitOfMeasureDto();
        dto.setId(uom.getId());
        dto.setUom(uom.getUom());
        return dto;
    }
}
