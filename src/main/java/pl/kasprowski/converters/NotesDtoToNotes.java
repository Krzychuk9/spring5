package pl.kasprowski.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.kasprowski.domain.Notes;
import pl.kasprowski.dto.NotesDto;

@Component
public class NotesDtoToNotes implements Converter<NotesDto, Notes> {

    @Synchronized
    @Nullable
    @Override
    public Notes convert(NotesDto dto) {
        if (dto == null) {
            return null;
        }
        final Notes notes = new Notes();
        notes.setId(dto.getId());
        notes.setRecipeNotes(dto.getRecipeNotes());
        return notes;
    }
}
