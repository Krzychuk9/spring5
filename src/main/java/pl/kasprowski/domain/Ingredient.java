package pl.kasprowski.domain;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(exclude = {"recipe"})
@Entity
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;
    private BigDecimal amount;
    @ManyToOne
    @JoinColumn(name = "recipe_id")
    private Recipe recipe;
    @OneToOne
    private UnitOfMeasure unitOfMeasure;

}
