package pl.kasprowski.domain;

public enum Difficulty {
    EASY("Easy"), MODERATE("Moderate"), HARD("Hard");

    Difficulty(String name) {
        this.name = name;
    }

    Difficulty() {
        this.name = this.toString();
    }

    public String getName() {
        return this.name;
    }

    private String name;
}
