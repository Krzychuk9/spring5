package pl.kasprowski.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.kasprowski.domain.Recipe;
import pl.kasprowski.repositories.RecipeRepository;

import java.io.IOException;
import java.util.Optional;

@Service
@Slf4j
public class ImageServiceImpl implements ImageService {

    private RecipeRepository recipeRepository;

    @Autowired
    public ImageServiceImpl(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    @Override
    public void saveImage(Long recipeId, MultipartFile file) {
        Optional<Recipe> recipeOpt = recipeRepository.findById(recipeId);

        if (recipeOpt.isPresent()) {
            Recipe recipe = recipeOpt.get();
            try {
                recipe.setImage(file.getBytes());
                recipeRepository.save(recipe);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            log.debug("Recipe not found");
        }
    }

    @Override
    public byte[] getImage(Long recipeId) {
        Optional<Recipe> recipeOpt = recipeRepository.findById(recipeId);
        return recipeOpt.map(Recipe::getImage).orElseGet(() -> new byte[0]);
    }
}
