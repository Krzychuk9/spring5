package pl.kasprowski.services;

import pl.kasprowski.dto.UnitOfMeasureDto;

import java.util.List;

public interface UnitOfMeasureService {
    List<UnitOfMeasureDto> findAllUOM();
}
