package pl.kasprowski.services;

import pl.kasprowski.dto.IngredientDto;

public interface IngredientService {
    IngredientDto findByRecipeIdAndId(Long recipeId, Long id);

    void saveOrUpdate(IngredientDto dto, Long recipeId);

    void deleteIngredient(Long recipeId, Long id);
}
