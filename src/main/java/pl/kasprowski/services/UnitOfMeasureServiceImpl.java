package pl.kasprowski.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kasprowski.converters.UnitOfMeasureToUnitOfMeasureDto;
import pl.kasprowski.dto.UnitOfMeasureDto;
import pl.kasprowski.repositories.UnitOfMeasureRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UnitOfMeasureServiceImpl implements UnitOfMeasureService {

    private UnitOfMeasureRepository uomRepository;
    private UnitOfMeasureToUnitOfMeasureDto converter;

    @Autowired
    public UnitOfMeasureServiceImpl(UnitOfMeasureRepository uomRepository, UnitOfMeasureToUnitOfMeasureDto converter) {
        this.uomRepository = uomRepository;
        this.converter = converter;
    }

    @Override
    public List<UnitOfMeasureDto> findAllUOM() {
        return StreamSupport.stream(uomRepository.findAll().spliterator(), false).
                map(converter::convert).
                collect(Collectors.toList());
    }
}
