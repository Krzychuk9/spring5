package pl.kasprowski.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.kasprowski.domain.Ingredient;
import pl.kasprowski.domain.Recipe;
import pl.kasprowski.dto.IngredientDto;
import pl.kasprowski.dto.RecipeDto;
import pl.kasprowski.repositories.RecipeRepository;

import java.util.Optional;
import java.util.Set;

@Service
public class IngredientServiceImpl implements IngredientService {

    private RecipeService recipeService;
    private RecipeRepository repository;

    @Autowired
    public IngredientServiceImpl(RecipeService recipeService, RecipeRepository repository) {
        this.recipeService = recipeService;
        this.repository = repository;
    }

    @Override
    public IngredientDto findByRecipeIdAndId(Long recipeId, Long id) {
        RecipeDto recipe = recipeService.findById(recipeId);
        Optional<IngredientDto> optIngredient = recipe.getIngredients().stream().filter(i -> id.equals(i.getId())).findFirst();
        return optIngredient.orElse(null);
    }

    @Override
    @Transactional
    public void saveOrUpdate(IngredientDto dto, Long recipeId) {
        RecipeDto recipe = recipeService.findById(recipeId);
        Set<IngredientDto> ingredients = recipe.getIngredients();

        if (dto.getId() != null) {
            Optional<IngredientDto> ingredientToRemove = ingredients.stream().filter(i -> i.getId().equals(dto.getId())).findFirst();
            ingredients.remove(ingredientToRemove.get());
        }

        ingredients.add(dto);
        recipeService.saveRecipe(recipe);
    }

    @Override
    @Transactional
    public void deleteIngredient(Long recipeId, Long id) {
        Optional<Recipe> recipeOpt = repository.findById(recipeId);
        if (recipeOpt.isPresent()) {
            Recipe recipe = recipeOpt.get();
            Set<Ingredient> ingredients = recipe.getIngredients();

            Optional<Ingredient> ingredientOpt = ingredients.stream().filter(i -> i.getId().equals(id)).findFirst();
            if (ingredientOpt.isPresent()) {
                Ingredient ingredient = ingredientOpt.get();
                ingredient.setRecipe(null);
                ingredients.remove(ingredient);
                repository.save(recipe);
            } else {
                throw new RuntimeException("Ingredient no found! ID: " + id);
            }

        } else {
            throw new RuntimeException("Recipe no found! ID: " + recipeId);
        }
    }
}
