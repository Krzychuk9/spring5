package pl.kasprowski.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kasprowski.converters.RecipeDtoToRecipe;
import pl.kasprowski.converters.RecipeToRecipeDto;
import pl.kasprowski.domain.Recipe;
import pl.kasprowski.dto.RecipeDto;
import pl.kasprowski.exeptions.NotFoundException;
import pl.kasprowski.repositories.RecipeRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
public class RecipeServiceImpl implements RecipeService {

    private RecipeRepository recipeRepository;
    private RecipeDtoToRecipe converter;
    private RecipeToRecipeDto converterToDto;

    @Autowired
    public RecipeServiceImpl(RecipeRepository recipeRepository, RecipeDtoToRecipe converter, RecipeToRecipeDto converterToDto) {
        this.recipeRepository = recipeRepository;
        this.converter = converter;
        this.converterToDto = converterToDto;
    }

    @Override
    public Set<RecipeDto> getAllRecipes() {
        log.debug("Getting all recipes!");
        Set<RecipeDto> recipes = new HashSet<>();
        recipeRepository.findAll().forEach(recipe -> recipes.add(converterToDto.convert(recipe)));
        return recipes;
    }

    @Override
    public RecipeDto findById(long id) {
        Optional<Recipe> recipe = recipeRepository.findById(id);
        if (recipe.isPresent()) {
            return converterToDto.convert(recipe.get());
        } else {
            throw new NotFoundException("Recipe not found. For id value: " + id);
        }
    }

    @Override
    public void saveRecipe(RecipeDto recipeDto) {
        Recipe recipe = converter.convert(recipeDto);
        recipeRepository.save(recipe);
    }

    @Override
    public void delete(Long id) {
        recipeRepository.deleteById(id);
    }
}
