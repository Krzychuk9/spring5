package pl.kasprowski.services;

import org.springframework.web.multipart.MultipartFile;

public interface ImageService {
    void saveImage(Long recipeId, MultipartFile file);

    byte[] getImage(Long recipeId);
}
