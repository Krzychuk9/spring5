package pl.kasprowski.services;

import pl.kasprowski.dto.RecipeDto;

import java.util.Set;

public interface RecipeService {
    Set<RecipeDto> getAllRecipes();

    RecipeDto findById(long id);

    void saveRecipe(RecipeDto recipeDto);

    void delete(Long id);
}
