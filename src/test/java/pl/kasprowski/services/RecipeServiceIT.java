package pl.kasprowski.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.kasprowski.converters.RecipeToRecipeDto;
import pl.kasprowski.domain.Recipe;
import pl.kasprowski.dto.RecipeDto;
import pl.kasprowski.repositories.RecipeRepository;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RecipeServiceIT {

    @Autowired
    private RecipeService recipeService;
    @Autowired
    private RecipeRepository recipeRepository;
    @Autowired
    private RecipeToRecipeDto converter;

    public static final String DESCRIPTION = "new description";

    @Test
    @Transactional
    public void saveNewDescriptionTest() throws Exception {
        Iterable<Recipe> recipes = recipeRepository.findAll();
        Recipe recipe = recipes.iterator().next();
        Long id = recipe.getId();
        RecipeDto dto = converter.convert(recipe);

        dto.setDescription(DESCRIPTION);
        recipeService.saveRecipe(dto);

        Optional<Recipe> optRecipe = recipeRepository.findById(id);
        Recipe newRecipe = optRecipe.get();

        assertNotNull(newRecipe);
        assertEquals(DESCRIPTION, newRecipe.getDescription());
        assertEquals(id, newRecipe.getId());
    }
}
