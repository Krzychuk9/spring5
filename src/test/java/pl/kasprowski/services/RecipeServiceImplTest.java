package pl.kasprowski.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.kasprowski.converters.RecipeDtoToRecipe;
import pl.kasprowski.converters.RecipeToRecipeDto;
import pl.kasprowski.domain.Recipe;
import pl.kasprowski.dto.RecipeDto;
import pl.kasprowski.exeptions.NotFoundException;
import pl.kasprowski.repositories.RecipeRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class RecipeServiceImplTest {

    @InjectMocks
    private RecipeServiceImpl recipeService;

    @Mock
    private RecipeRepository recipeRepository;

    @Mock
    private RecipeDtoToRecipe converter;

    @Mock
    private RecipeToRecipeDto converterToDto;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getRecipeById() throws Exception {
        Recipe recipe = new Recipe();
        recipe.setId(1L);
        Optional<Recipe> recipeOptional = Optional.of(recipe);
        RecipeDto dto = new RecipeDto();
        dto.setId(1L);
        when(converterToDto.convert(any(Recipe.class))).thenReturn(dto);

        when(recipeRepository.findById(anyLong())).thenReturn(recipeOptional);

        RecipeDto returnedRecipe = recipeService.findById(1L);

        assertNotNull(returnedRecipe);
        verify(recipeRepository, times(1)).findById(anyLong());
        verify(recipeRepository, never()).findAll();
    }

    @Test(expected = NotFoundException.class)
    public void findByID_shouldThrowException() throws Exception {
        when(recipeRepository.findById(anyLong())).thenReturn(Optional.empty());

        recipeService.findById(1L);
    }

    @Test
    public void getAllRecipes() throws Exception {
        Recipe recipe = new Recipe();
        Set<Recipe> recipes = new HashSet<>();
        recipes.add(recipe);
        RecipeDto dto = new RecipeDto();
        when(converterToDto.convert(any(Recipe.class))).thenReturn(dto);

        when(recipeRepository.findAll()).thenReturn(recipes);

        Set<RecipeDto> allRecipes = recipeService.getAllRecipes();

        verify(recipeRepository, times(1)).findAll();
        assertNotNull(allRecipes);
        allRecipes.forEach(Assert::assertNotNull);
        assertEquals(1, allRecipes.size());
    }

    @Test
    public void saveRecipe() throws Exception {
        RecipeDto dto = new RecipeDto();
        Recipe recipe = new Recipe();
        when(converter.convert(dto)).thenReturn(recipe);

        recipeService.saveRecipe(dto);

        verify(converter, times(1)).convert(dto);
        verify(recipeRepository, times(1)).save(recipe);
    }

    @Test
    public void deleteRecipe() throws Exception {
        long id = 1L;

        recipeService.delete(id);

        verify(recipeRepository, times(1)).deleteById(id);
    }
}