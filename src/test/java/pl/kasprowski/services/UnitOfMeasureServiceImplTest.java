package pl.kasprowski.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.kasprowski.converters.UnitOfMeasureToUnitOfMeasureDto;
import pl.kasprowski.domain.UnitOfMeasure;
import pl.kasprowski.dto.UnitOfMeasureDto;
import pl.kasprowski.repositories.UnitOfMeasureRepository;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UnitOfMeasureServiceImplTest {

    @Mock
    private UnitOfMeasureRepository repository;
    private UnitOfMeasureToUnitOfMeasureDto converter;
    private UnitOfMeasureService service;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        converter = new UnitOfMeasureToUnitOfMeasureDto();
        service = new UnitOfMeasureServiceImpl(repository, converter);
    }

    @Test
    public void findAllUOM() throws Exception {
        UnitOfMeasure uom1 = new UnitOfMeasure();
        uom1.setUom("uom1");
        uom1.setId(1L);
        UnitOfMeasure uom2 = new UnitOfMeasure();
        uom1.setUom("uom2");
        uom1.setId(2L);
        List<UnitOfMeasure> unitOfMeasures = Arrays.asList(uom1, uom2);
        when(repository.findAll()).thenReturn(unitOfMeasures);


        List<UnitOfMeasureDto> uoms = service.findAllUOM();

        assertNotNull(uoms);
        assertEquals(2, uoms.size());
        assertTrue(uoms.stream().anyMatch(u -> u.getId().equals(2L)));
        verify(repository, times(1)).findAll();
    }
}