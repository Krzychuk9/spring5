package pl.kasprowski.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockMultipartFile;
import pl.kasprowski.domain.Recipe;
import pl.kasprowski.repositories.RecipeRepository;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;


public class ImageServiceImplTest {

    @Mock
    private RecipeRepository recipeRepository;
    @InjectMocks
    private ImageServiceImpl imageService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void saveImage() throws Exception {
        Recipe recipe = new Recipe();
        Optional<Recipe> recipeOpt = Optional.of(recipe);
        when(recipeRepository.findById(anyLong())).thenReturn(recipeOpt);

        MockMultipartFile file =
                new MockMultipartFile("imageFile", "test.txt", "text/plain", "Super test".getBytes());

        ArgumentCaptor<Recipe> argumentCaptor = ArgumentCaptor.forClass(Recipe.class);

        imageService.saveImage(1L, file);

        verify(recipeRepository, times(1)).save(argumentCaptor.capture());
        Recipe savedRecipe = argumentCaptor.getValue();
        assertEquals(file.getBytes().length, savedRecipe.getImage().length);

    }

    @Test
    public void getImage() throws Exception {
        byte[] bytes = "SuperTest".getBytes();
        Recipe recipe = new Recipe();
        recipe.setImage(bytes);
        Optional<Recipe> recipeOpt = Optional.of(recipe);
        when(recipeRepository.findById(anyLong())).thenReturn(recipeOpt);

        byte[] image = imageService.getImage(1L);

        assertEquals(bytes, image);
    }

}