package pl.kasprowski.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.kasprowski.domain.Ingredient;
import pl.kasprowski.domain.Recipe;
import pl.kasprowski.dto.IngredientDto;
import pl.kasprowski.dto.RecipeDto;
import pl.kasprowski.repositories.RecipeRepository;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class IngredientServiceImplTest {

    @Mock
    private RecipeService recipeService;
    @Mock
    private RecipeRepository recipeRepository;

    @InjectMocks
    private IngredientServiceImpl ingredientService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findByRecipeIdAndId() throws Exception {
        Long recipeId = 2L;
        Long ingredientId = 1L;
        RecipeDto recipeDto = new RecipeDto();
        recipeDto.setId(recipeId);
        recipeDto.setIngredients(new HashSet<>());
        IngredientDto dto1 = new IngredientDto();
        dto1.setId(ingredientId);
        dto1.setRecipeId(recipeId);
        IngredientDto dto2 = new IngredientDto();
        dto2.setRecipeId(recipeId);
        recipeDto.getIngredients().add(dto1);
        recipeDto.getIngredients().add(dto2);

        when(recipeService.findById(recipeId)).thenReturn(recipeDto);

        IngredientDto ingredient = ingredientService.findByRecipeIdAndId(recipeId, ingredientId);

        assertNotNull(ingredient);
        assertEquals(ingredientId, ingredient.getId());
        assertEquals(recipeId, ingredient.getRecipeId());
    }

    @Test
    public void deleteIngredient() throws Exception {
        Recipe recipe = new Recipe();
        Ingredient ingredient = new Ingredient();
        ingredient.setId(3L);
        recipe.setIngredients(new HashSet<>());
        recipe.addIngridient(ingredient);
        Optional<Recipe> recipeOpt = Optional.of(recipe);

        when(recipeRepository.findById(anyLong())).thenReturn(recipeOpt);

        ingredientService.deleteIngredient(1L, 3L);

        verify(recipeRepository, times(1)).findById(1L);
        verify(recipeRepository, times(1)).save(any(Recipe.class));
    }
}