package pl.kasprowski.controllers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.kasprowski.dto.RecipeDto;
import pl.kasprowski.exeptions.NotFoundException;
import pl.kasprowski.services.RecipeService;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RecipeControllerTest {

    @Mock
    private RecipeService recipeService;
    @InjectMocks
    private RecipeController recipeController;
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(recipeController)
                .setControllerAdvice(new ExceptionHandlerController())
                .build();
    }

    @Test
    public void getRecipe() throws Exception {
        RecipeDto recipe = new RecipeDto();
        recipe.setId(1L);

        when(recipeService.findById(anyLong())).thenReturn(recipe);

        mockMvc.perform(get("/recipe/show/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/show"))
                .andExpect(model().attributeExists("recipe"));

        verify(recipeService, times(1)).findById(anyLong());
    }

    @Test
    public void getRecipeNotFound() throws Exception {
        when(recipeService.findById(anyLong())).thenThrow(NotFoundException.class);

        mockMvc.perform(get("/recipe/show/5"))
                .andExpect(status().isNotFound())
                .andExpect(view().name("404error"));
    }

    @Test
    public void showRecipeForm() throws Exception {

        mockMvc.perform(get("/recipe"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/recipeForm"))
                .andExpect(model().attributeExists("recipe"));

        verifyZeroInteractions(recipeService);
    }

    @Test
    public void saveRecipe() throws Exception {
        ArgumentCaptor<RecipeDto> argumentCaptor = ArgumentCaptor.forClass(RecipeDto.class);

        mockMvc.perform(post("/recipe")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "")
                .param("description", "some description")
                .param("preTime", "5")
                .param("cookTime", "55")
                .param("servings", "5")
                .param("directions", "abc"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));

        verify(recipeService, times(1)).saveRecipe(argumentCaptor.capture());
        assertEquals("some description", argumentCaptor.getValue().getDescription());
    }

    @Test
    public void saveRecipeWithValidationErrors() throws Exception {

        mockMvc.perform(post("/recipe")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "")
                .param("description", "some description"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/recipeForm"));

        verifyZeroInteractions(recipeService);
    }

    @Test
    public void updateRecipeForm() throws Exception {
        RecipeDto dto = new RecipeDto();
        dto.setId(1L);

        when(recipeService.findById(1L)).thenReturn(dto);

        mockMvc.perform(get("/recipe/1/update"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/recipeForm"))
                .andExpect(model().attributeExists("recipe"));

        verify(recipeService, times(1)).findById(anyLong());
    }

    @Test
    public void deleteRecipe() throws Exception {

        mockMvc.perform(get("/recipe/1/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));

        verify(recipeService, times(1)).delete(1L);
    }

    @Test
    public void handleNumberFormatException() throws Exception {

        mockMvc.perform(get("/recipe/show/abc"))
                .andExpect(status().isBadRequest())
                .andExpect(view().name("400error"));
    }
}