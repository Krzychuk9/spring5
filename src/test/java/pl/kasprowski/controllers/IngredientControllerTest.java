package pl.kasprowski.controllers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.kasprowski.dto.IngredientDto;
import pl.kasprowski.dto.RecipeDto;
import pl.kasprowski.services.IngredientService;
import pl.kasprowski.services.RecipeService;
import pl.kasprowski.services.UnitOfMeasureService;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class IngredientControllerTest {

    @Mock
    private RecipeService recipeService;

    @Mock
    private IngredientService ingredientService;

    @Mock
    private UnitOfMeasureService uomService;

    @InjectMocks
    private IngredientController controller;

    private MockMvc mockMvc;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setControllerAdvice(new ExceptionHandlerController())
                .build();
    }

    @Test
    public void listIngredients() throws Exception {
        RecipeDto dto = new RecipeDto();
        when(recipeService.findById(anyLong())).thenReturn(dto);

        mockMvc.perform(get("/recipe/1/ingredients"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/ingredient/list"))
                .andExpect(model().attributeExists("recipe"));

        verify(recipeService, times(1)).findById(1L);
    }

    @Test
    public void deleteIngredient() throws Exception {
        mockMvc.perform(get("/recipe/1/ingredients/2/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/recipe/1/ingredients"));

        verify(ingredientService, times(1)).deleteIngredient(eq(1L), eq(2L));
    }

    @Test
    public void showIngredient() throws Exception {
        IngredientDto dto = new IngredientDto();
        when(ingredientService.findByRecipeIdAndId(anyLong(), anyLong())).thenReturn(dto);

        mockMvc.perform(get("/recipe/1/ingredients/1/show"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/ingredient/show"))
                .andExpect(model().attributeExists("ingredient"));

        verify(ingredientService, times(1)).findByRecipeIdAndId(1L, 1L);
    }

    @Test
    public void updateIngredient() throws Exception {
        when(ingredientService.findByRecipeIdAndId(anyLong(), anyLong())).thenReturn(new IngredientDto());
        when(uomService.findAllUOM()).thenReturn(new ArrayList<>());

        mockMvc.perform(get("/recipe/1/ingredients/2/update"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/ingredient/ingredientform"))
                .andExpect(model().attributeExists("ingredient"))
                .andExpect(model().attributeExists("uomList"));

        verify(ingredientService, times(1)).findByRecipeIdAndId(eq(1L), eq(2L));
        verify(uomService, times(1)).findAllUOM();
    }

    @Test
    public void createIngredient() throws Exception {
        when(uomService.findAllUOM()).thenReturn(new ArrayList<>());

        mockMvc.perform(get("/recipe/1/ingredients/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/ingredient/ingredientform"))
                .andExpect(model().attributeExists("ingredient"))
                .andExpect(model().attributeExists("uomList"));

        verify(uomService, times(1)).findAllUOM();
        verifyZeroInteractions(recipeService);
    }

    @Test
    public void saveOrUpdateIngredient() throws Exception {
        ArgumentCaptor<IngredientDto> argumentCaptor = ArgumentCaptor.forClass(IngredientDto.class);

        mockMvc.perform(post("/recipe/2/ingredients").
                contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "")
                .param("recipeId", "2")
                .param("description", "description")
                .param("amount", "5")
                .param("unitOfMeasure.id", "2"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/recipe/2/ingredients"));

        verify(ingredientService).saveOrUpdate(argumentCaptor.capture(), eq(2L));
        assertEquals(new BigDecimal("5"), argumentCaptor.getValue().getAmount());
    }

    @Test
    public void handleNumberFormatException() throws Exception {

        mockMvc.perform(get("/recipe/1/ingredients/abv/show"))
                .andExpect(status().isBadRequest())
                .andExpect(view().name("400error"));
    }
}