package pl.kasprowski.controllers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.kasprowski.services.ImageService;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ImageControllerTest {

    @Mock
    private ImageService imageService;
    @InjectMocks
    private ImageController imageController;
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(imageController).build();
    }

    @Test
    public void showImageForm() throws Exception {
        mockMvc.perform(get("/recipe/1/image"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/imageuploadform"))
                .andExpect(model().attributeExists("recipeId"));
    }

    @Test
    public void uploadFile() throws Exception {
        MockMultipartFile file =
                new MockMultipartFile("imageFile", "test.txt", "text/plain", "Super test".getBytes());

        mockMvc.perform(multipart("/recipe/1/image").file(file))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/recipe/show/1"));

        verify(imageService, times(1)).saveImage(anyLong(), any());
    }

    @Test
    public void renderImageFromDB() throws Exception {
        byte[] bytes = "Super test".getBytes();

        when(imageService.getImage(1L)).thenReturn(bytes);

        MockHttpServletResponse response = mockMvc.perform(get("/recipe/1/image/show"))
                .andExpect(status().isOk())
                .andReturn().getResponse();

        byte[] contentAsByteArray = response.getContentAsByteArray();

        assertEquals(bytes.length, contentAsByteArray.length);
    }
}