package pl.kasprowski.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CategoryTest {

    private Category category;

    @Before
    public void setUp() throws Exception {
        category = new Category();
    }

    @Test
    public void getId() throws Exception {
        Long idValue = 4l;

        category.setId(idValue);

        assertEquals(idValue, category.getId());
    }

    @Test
    public void getCategoryName() throws Exception {
        String categoryName = "category";

        category.setCategoryName(categoryName);

        assertEquals(categoryName, category.getCategoryName());
    }
}