package pl.kasprowski.converters;

import org.junit.Before;
import org.junit.Test;
import pl.kasprowski.domain.Category;
import pl.kasprowski.dto.CategoryDto;

import static org.junit.Assert.*;

public class CategoryDtoToCategoryTest {

    private CategoryDtoToCategory converter;

    private static final Long ID = 1L;
    private static final String DESCRIPTION = "description";

    @Before
    public void setUp() throws Exception {
        converter = new CategoryDtoToCategory();
    }

    @Test
    public void convertNull() throws Exception {
        Category result = converter.convert(null);

        assertNull(result);
    }

    @Test
    public void convertEmptyObject() throws Exception {
        Category result = converter.convert(new CategoryDto());

        assertNotNull(result);
    }

    @Test
    public void convert() throws Exception {
        CategoryDto dto = new CategoryDto();
        dto.setId(ID);
        dto.setCategoryName(DESCRIPTION);

        Category result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(ID, result.getId());
        assertEquals(DESCRIPTION, result.getCategoryName());
    }
}