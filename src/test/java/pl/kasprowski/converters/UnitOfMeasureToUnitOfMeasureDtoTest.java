package pl.kasprowski.converters;

import org.junit.Before;
import org.junit.Test;
import pl.kasprowski.domain.UnitOfMeasure;
import pl.kasprowski.dto.UnitOfMeasureDto;

import static org.junit.Assert.*;

public class UnitOfMeasureToUnitOfMeasureDtoTest {

    private UnitOfMeasureToUnitOfMeasureDto converter;

    private static final Long ID = 1L;
    private static final String UOM = "uom";

    @Before
    public void setUp() throws Exception {
        converter = new UnitOfMeasureToUnitOfMeasureDto();
    }

    @Test
    public void convertNull() throws Exception {
        UnitOfMeasureDto result = converter.convert(null);

        assertNull(result);
    }

    @Test
    public void convertEmptyObject() throws Exception {
        UnitOfMeasureDto result = converter.convert(new UnitOfMeasure());

        assertNotNull(result);
    }

    @Test
    public void convert() throws Exception {
        UnitOfMeasure uom = new UnitOfMeasure();
        uom.setId(ID);
        uom.setUom(UOM);

        UnitOfMeasureDto result = converter.convert(uom);

        assertNotNull(result);
        assertEquals(ID, result.getId());
        assertEquals(UOM, result.getUom());
    }

}