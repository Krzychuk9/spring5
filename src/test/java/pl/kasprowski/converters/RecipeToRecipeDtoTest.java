package pl.kasprowski.converters;

import org.junit.Before;
import org.junit.Test;
import pl.kasprowski.domain.*;
import pl.kasprowski.dto.RecipeDto;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class RecipeToRecipeDtoTest {

    private RecipeToRecipeDto converter;

    private static final Long ID = 1L;
    private static final String DESCRIPTION = "description";
    private static final Integer PRE_TIME = 10;
    private static final Integer COOK_TIME = 20;
    private static final Integer SERVINGS = 5;
    private static final String SOURCE = "source";
    private static final String URL = "HTTP";
    private static final String DIRECTIONS = "directions";
    private static final Difficulty DIFFICULTY = Difficulty.HARD;
    private static final Notes NOTES = new Notes();
    private static final Set<Ingredient> INGREDIENTS = new HashSet<>();
    private static final Set<Category> CATEGORIES = new HashSet<>();
    private static final String RECIPE_NOTES = "recipe notes";

    @Before
    public void setUp() throws Exception {
        converter = new RecipeToRecipeDto(new IngredientToIngredientDto(new UnitOfMeasureToUnitOfMeasureDto()), new NotesToNotesDto(), new CategoryToCategoryDto());
        NOTES.setId(ID);
        NOTES.setRecipeNotes(RECIPE_NOTES);
        CATEGORIES.add(new Category());
        INGREDIENTS.add(new Ingredient());
    }

    @Test
    public void convertNull() throws Exception {
        RecipeDto result = converter.convert(null);

        assertNull(result);
    }

    @Test
    public void convertEmptyObject() throws Exception {
        RecipeDto result = converter.convert(new Recipe());

        assertNotNull(result);
    }

    @Test
    public void convert() throws Exception {
        Recipe recipe = new Recipe();
        recipe.setId(ID);
        recipe.setDescription(DESCRIPTION);
        recipe.setPreTime(PRE_TIME);
        recipe.setCookTime(COOK_TIME);
        recipe.setServings(SERVINGS);
        recipe.setSource(SOURCE);
        recipe.setUrl(URL);
        recipe.setDirections(DIRECTIONS);
        recipe.setIngredients(INGREDIENTS);
        recipe.setDifficulty(DIFFICULTY);
        recipe.setNotes(NOTES);
        recipe.setCategories(CATEGORIES);

        RecipeDto result = converter.convert(recipe);

        assertNotNull(result);
        assertEquals(ID, result.getId());
        assertEquals(DESCRIPTION, result.getDescription());
        assertEquals(PRE_TIME, result.getPreTime());
        assertEquals(COOK_TIME, result.getCookTime());
        assertEquals(SERVINGS, result.getServings());
        assertEquals(SOURCE, result.getSource());
        assertEquals(URL, result.getUrl());
        assertEquals(DIRECTIONS, result.getDirections());
        assertEquals(DIFFICULTY, result.getDifficulty());
        assertNotNull(result.getIngredients());
        assertNotNull(result.getCategories());
        assertNotNull(result.getNotes());
        assertEquals(1, result.getIngredients().size());
        assertEquals(1, result.getCategories().size());
        assertEquals(RECIPE_NOTES, result.getNotes().getRecipeNotes());
    }

}