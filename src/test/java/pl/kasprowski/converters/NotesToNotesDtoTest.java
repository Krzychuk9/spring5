package pl.kasprowski.converters;

import org.junit.Before;
import org.junit.Test;
import pl.kasprowski.domain.Notes;
import pl.kasprowski.dto.NotesDto;

import static org.junit.Assert.*;

public class NotesToNotesDtoTest {

    private NotesToNotesDto converter;

    private static final Long ID = 1L;
    private static final String RECIPE_NOTES = "notes";

    @Before
    public void setUp() throws Exception {
        converter = new NotesToNotesDto();
    }


    @Test
    public void convertNull() throws Exception {
        NotesDto result = converter.convert(null);

        assertNull(result);
    }

    @Test
    public void convertEmptyObject() throws Exception {
        NotesDto result = converter.convert(new Notes());

        assertNotNull(result);
    }

    @Test
    public void convert() throws Exception {
        Notes notes = new Notes();
        notes.setId(ID);
        notes.setRecipeNotes(RECIPE_NOTES);

        NotesDto result = converter.convert(notes);

        assertNotNull(result);
        assertEquals(ID, result.getId());
        assertEquals(RECIPE_NOTES, result.getRecipeNotes());
    }

}