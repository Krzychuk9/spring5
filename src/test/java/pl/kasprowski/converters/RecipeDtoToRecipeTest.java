package pl.kasprowski.converters;

import org.junit.Before;
import org.junit.Test;
import pl.kasprowski.domain.Difficulty;
import pl.kasprowski.domain.Recipe;
import pl.kasprowski.dto.CategoryDto;
import pl.kasprowski.dto.IngredientDto;
import pl.kasprowski.dto.NotesDto;
import pl.kasprowski.dto.RecipeDto;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class RecipeDtoToRecipeTest {

    private RecipeDtoToRecipe converter;

    private static final Long ID = 1L;
    private static final String DESCRIPTION = "description";
    private static final Integer PRE_TIME = 10;
    private static final Integer COOK_TIME = 20;
    private static final Integer SERVINGS = 5;
    private static final String SOURCE = "source";
    private static final String URL = "HTTP";
    private static final String DIRECTIONS = "directions";
    private static final Difficulty DIFFICULTY = Difficulty.HARD;
    private static final NotesDto NOTES = new NotesDto();
    private static final Set<IngredientDto> INGREDIENTS = new HashSet<>();
    private static final Set<CategoryDto> CATEGORIES = new HashSet<>();
    private static final String RECIPE_NOTES = "recipe notes";

    @Before
    public void setUp() throws Exception {
        converter = new RecipeDtoToRecipe(new IngredientDtoToIngredient(new UnitOfMeasureDtoToUnitOfMeasure()), new NotesDtoToNotes(), new CategoryDtoToCategory());
        NOTES.setId(ID);
        NOTES.setRecipeNotes(RECIPE_NOTES);
        CATEGORIES.add(new CategoryDto());
        INGREDIENTS.add(new IngredientDto());
    }


    @Test
    public void convertNull() throws Exception {
        Recipe result = converter.convert(null);

        assertNull(result);
    }

    @Test
    public void convertEmptyObject() throws Exception {
        Recipe result = converter.convert(new RecipeDto());

        assertNotNull(result);
    }

    @Test
    public void convert() throws Exception {
        RecipeDto recipe = new RecipeDto();
        recipe.setId(ID);
        recipe.setDescription(DESCRIPTION);
        recipe.setPreTime(PRE_TIME);
        recipe.setCookTime(COOK_TIME);
        recipe.setServings(SERVINGS);
        recipe.setSource(SOURCE);
        recipe.setUrl(URL);
        recipe.setDirections(DIRECTIONS);
        recipe.setIngredients(INGREDIENTS);
        recipe.setDifficulty(DIFFICULTY);
        recipe.setNotes(NOTES);
        recipe.setCategories(CATEGORIES);

        Recipe result = converter.convert(recipe);

        assertNotNull(result);
        assertEquals(ID, result.getId());
        assertEquals(DESCRIPTION, result.getDescription());
        assertEquals(PRE_TIME, result.getPreTime());
        assertEquals(COOK_TIME, result.getCookTime());
        assertEquals(SERVINGS, result.getServings());
        assertEquals(SOURCE, result.getSource());
        assertEquals(URL, result.getUrl());
        assertEquals(DIRECTIONS, result.getDirections());
        assertEquals(DIFFICULTY, result.getDifficulty());
        assertNotNull(result.getIngredients());
        assertNotNull(result.getCategories());
        assertNotNull(result.getNotes());
        assertEquals(1, result.getIngredients().size());
        assertEquals(1, result.getCategories().size());
        assertEquals(RECIPE_NOTES, result.getNotes().getRecipeNotes());
    }

}