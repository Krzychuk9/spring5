package pl.kasprowski.converters;

import org.junit.Before;
import org.junit.Test;
import pl.kasprowski.domain.Ingredient;
import pl.kasprowski.dto.IngredientDto;
import pl.kasprowski.dto.UnitOfMeasureDto;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class IngredientDtoToIngredientTest {

    private IngredientDtoToIngredient converter;
    private UnitOfMeasureDto uom;

    private static final Long ID = 1L;
    private static final String DESCRIPTION = "description";
    private static final BigDecimal AMOUNT = new BigDecimal("1");
    private static final String UOM = "uom";

    @Before
    public void setUp() throws Exception {
        converter = new IngredientDtoToIngredient(new UnitOfMeasureDtoToUnitOfMeasure());
        uom = new UnitOfMeasureDto();
        uom.setId(ID);
        uom.setUom(UOM);
    }

    @Test
    public void convertNull() throws Exception {
        Ingredient result = converter.convert(null);

        assertNull(result);
    }

    @Test
    public void convertEmptyObject() throws Exception {
        Ingredient result = converter.convert(new IngredientDto());

        assertNotNull(result);
    }

    @Test
    public void convert() throws Exception {
        IngredientDto dto = new IngredientDto();
        dto.setId(ID);
        dto.setUnitOfMeasure(uom);
        dto.setDescription(DESCRIPTION);
        dto.setAmount(AMOUNT);

        Ingredient result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(ID, result.getId());
        assertEquals(DESCRIPTION, result.getDescription());
        assertEquals(AMOUNT, result.getAmount());
        assertEquals(UOM, result.getUnitOfMeasure().getUom());
    }

    @Test
    public void convertWithNullUOM() throws Exception {
        IngredientDto dto = new IngredientDto();
        dto.setId(ID);
        dto.setDescription(DESCRIPTION);
        dto.setAmount(AMOUNT);

        Ingredient result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(ID, result.getId());
        assertEquals(DESCRIPTION, result.getDescription());
        assertEquals(AMOUNT, result.getAmount());
        assertNull(result.getUnitOfMeasure());
    }
}