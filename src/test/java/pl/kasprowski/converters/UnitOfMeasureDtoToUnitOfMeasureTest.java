package pl.kasprowski.converters;

import org.junit.Before;
import org.junit.Test;
import pl.kasprowski.domain.UnitOfMeasure;
import pl.kasprowski.dto.UnitOfMeasureDto;

import static org.junit.Assert.*;

public class UnitOfMeasureDtoToUnitOfMeasureTest {

    private UnitOfMeasureDtoToUnitOfMeasure converter;

    private static final Long ID = 1L;
    private static final String UOM = "uom";

    @Before
    public void setUp() throws Exception {
        converter = new UnitOfMeasureDtoToUnitOfMeasure();
    }

    @Test
    public void convertNull() throws Exception {
        UnitOfMeasure result = converter.convert(null);

        assertNull(result);
    }

    @Test
    public void convertEmptyObject() throws Exception {
        UnitOfMeasure result = converter.convert(new UnitOfMeasureDto());

        assertNotNull(result);
    }

    @Test
    public void convert() throws Exception {
        UnitOfMeasureDto dto = new UnitOfMeasureDto();
        dto.setId(ID);
        dto.setUom(UOM);

        UnitOfMeasure result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(ID, result.getId());
        assertEquals(UOM, result.getUom());
    }
}