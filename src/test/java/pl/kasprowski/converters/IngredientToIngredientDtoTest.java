package pl.kasprowski.converters;

import org.junit.Before;
import org.junit.Test;
import pl.kasprowski.domain.Ingredient;
import pl.kasprowski.domain.UnitOfMeasure;
import pl.kasprowski.dto.IngredientDto;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class IngredientToIngredientDtoTest {

    private IngredientToIngredientDto converter;
    private UnitOfMeasure uom;

    private static final Long ID = 1L;
    private static final String DESCRIPTION = "description";
    private static final BigDecimal AMOUNT = new BigDecimal("1");
    private static final String UOM = "uom";

    @Before
    public void setUp() throws Exception {
        converter = new IngredientToIngredientDto(new UnitOfMeasureToUnitOfMeasureDto());
        uom = new UnitOfMeasure();
        uom.setId(ID);
        uom.setUom(UOM);
    }

    @Test
    public void convertNull() throws Exception {
        IngredientDto result = converter.convert(null);

        assertNull(result);
    }

    @Test
    public void convertEmptyObject() throws Exception {
        IngredientDto result = converter.convert(new Ingredient());

        assertNotNull(result);
    }

    @Test
    public void convert() throws Exception {
        Ingredient ingredient = new Ingredient();
        ingredient.setId(ID);
        ingredient.setUnitOfMeasure(uom);
        ingredient.setDescription(DESCRIPTION);
        ingredient.setAmount(AMOUNT);

        IngredientDto result = converter.convert(ingredient);

        assertNotNull(result);
        assertEquals(ID, result.getId());
        assertEquals(DESCRIPTION, result.getDescription());
        assertEquals(AMOUNT, result.getAmount());
        assertEquals(UOM, result.getUnitOfMeasure().getUom());
    }

    @Test
    public void convertWithNullUOM() throws Exception {
        Ingredient ingredient = new Ingredient();
        ingredient.setId(ID);
        ingredient.setDescription(DESCRIPTION);
        ingredient.setAmount(AMOUNT);

        IngredientDto result = converter.convert(ingredient);

        assertNotNull(result);
        assertEquals(ID, result.getId());
        assertEquals(DESCRIPTION, result.getDescription());
        assertEquals(AMOUNT, result.getAmount());
        assertNull(result.getUnitOfMeasure());
    }
}