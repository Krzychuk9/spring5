package pl.kasprowski.converters;

import org.junit.Before;
import org.junit.Test;
import pl.kasprowski.domain.Notes;
import pl.kasprowski.dto.NotesDto;

import static org.junit.Assert.*;

public class NotesDtoToNotesTest {

    private NotesDtoToNotes converter;

    private static final Long ID = 1L;
    private static final String RECIPE_NOTES = "notes";

    @Before
    public void setUp() throws Exception {
        converter = new NotesDtoToNotes();
    }

    @Test
    public void convertNull() throws Exception {
        Notes result = converter.convert(null);

        assertNull(result);
    }

    @Test
    public void convertEmptyObject() throws Exception {
        Notes result = converter.convert(new NotesDto());

        assertNotNull(result);
    }

    @Test
    public void convert() throws Exception {
        NotesDto dto = new NotesDto();
        dto.setId(ID);
        dto.setRecipeNotes(RECIPE_NOTES);

        Notes result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(ID, result.getId());
        assertEquals(RECIPE_NOTES, result.getRecipeNotes());
    }
}