package pl.kasprowski.converters;

import org.junit.Before;
import org.junit.Test;
import pl.kasprowski.domain.Category;
import pl.kasprowski.dto.CategoryDto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class CategoryToCategoryDtoTest {

    private CategoryToCategoryDto converter;

    private static final Long ID = 1L;
    private static final String DESCRIPTION = "description";

    @Before
    public void setUp() throws Exception {
        converter = new CategoryToCategoryDto();
    }

    @Test
    public void convertNull() throws Exception {
        CategoryDto result = converter.convert(null);

        assertNull(result);
    }

    @Test
    public void convertEmptyObject() throws Exception {
        CategoryDto result = converter.convert(new Category());

        assertNotNull(result);
    }

    @Test
    public void convert() throws Exception {
        Category category = new Category();
        category.setId(ID);
        category.setCategoryName(DESCRIPTION);

        CategoryDto result = converter.convert(category);

        assertNotNull(result);
        assertEquals(ID, result.getId());
        assertEquals(DESCRIPTION, result.getCategoryName());
    }
}